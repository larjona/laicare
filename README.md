# LaiCare

[![build status](https://gitlab.com/frangor/laicare/badges/master/build.svg)]
    (https://gitlab.com/frangor/laicare/commits/master)

Android app to manage the care of dogs and cats.

<img src="https://gitlab.com/frangor/laicare/raw/master/screenshots.png"
    width="800" alt="LaiCare screenshots"/>

[<img src="https://f-droid.org/badge/get-it-on.png"
      alt="Get it on F-Droid"
      height="80">](https://f-droid.org/app/info.frangor.laicare)

## Features

 * Dogs and cats manager.
 * Treatments manager.
 * Appointments manager with reminder notifications.
 * Weight control with monitoring chart.

## Build requeriments

 * openjdk-7-jdk
 * android-sdk

## Build instructions

 * On GNU/Linux:

        ./gradlew assembleDebug

 * On Windows:

        gradlew.bat assembleDebug
